﻿using System.ComponentModel;
using System.Xml.Serialization;

namespace AutoParking.Api.Models
{
    public class Body
    {
        [XmlAttribute("SERVICE_ID")]
        [DefaultValue(0)]
        public int ServiceId { get; set; }

        [XmlAttribute("PARAM1")]
        public string AccountNumber { get; set; }

        [XmlAttribute("SUM")]
        [DefaultValue(0)]
        public decimal Sum { get; set; }

        [XmlAttribute("STATUS")]
        public int Status { get; set; }

        [XmlAttribute("MSG")]
        public string Message { get; set; }

        [XmlAttribute("ERR_MSG")]
        public string ErrorMessage { get; set; }
    }
}