﻿using System.Xml.Serialization;

namespace AutoParking.Api.Models
{
    [XmlRoot("XML")]
    public class PaymentRequestModel
    {
        [XmlElement("HEAD")]
        public Head Head { get; set; }


        [XmlElement("BODY")]
        public Body Body { get; set; }
    }
}