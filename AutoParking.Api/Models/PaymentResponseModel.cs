﻿using System.Xml.Serialization;

namespace AutoParking.Api.Models
{
    [XmlRoot("XML")]
    public class PaymentResponseModel
    {
        public PaymentResponseModel()
        {
            Head = new Head();
            Body = new Body();
        }
        [XmlElement("HEAD")]
        public Head Head { get; set; }


        [XmlElement("BODY")]
        public Body Body { get; set; }
    }

    
}