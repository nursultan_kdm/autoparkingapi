﻿using System.Collections.Generic;

namespace AutoParking.Api.Models
{
    public class TicketData
    {
        public static List<TicketModel> GetData()
        {
            return new List<TicketModel>()
            {
                new TicketModel() { TicketNumber = 5040317103946890, ClientName = "Иванов Сергей", RecommendPaymentSum = 350, CanReceivePayment = true },
                new TicketModel() { TicketNumber = 5040317104124588, ClientName = "Петров Иван", RecommendPaymentSum = 500, CanReceivePayment = true },
                new TicketModel() { TicketNumber = 5040317104224006, ClientName = "Штырь Лёха", RecommendPaymentSum = 400, CanReceivePayment = false },
                new TicketModel() { TicketNumber = 5040317105328815, ClientName = "Чесноков Витя", RecommendPaymentSum = 150, CanReceivePayment = false }
            };
        }
    }

    public class TicketModel
    {
        public long TicketNumber { get; set; }
        public string ClientName { get; set; }
        public decimal RecommendPaymentSum { get; set; }
        public bool CanReceivePayment { get; set; }
    }
}