﻿using System;
using System.Xml.Serialization;

namespace AutoParking.Api.Models
{
    public class Head
    {
        [XmlAttribute("OP")]
        public string Op { get; set; }

        [XmlIgnore]
        public DateTime Date { get; set; }

        [XmlAttribute("DTS")]
        public string DateString
        {
            get => Date.ToString("yyyy-MM-dd HH:mm:ss");
            set => Date = DateTime.Parse(value);
        }

        [XmlAttribute("QID")]
        public string Qid { get; set; }

        [XmlAttribute("QM")]
        public string Qm { get; set; }
    }
}