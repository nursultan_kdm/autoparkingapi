﻿using System.Web.Mvc;
using Ninject;

namespace AutoParking.Api.DI
{
    public class NinjectBootstrapper
    {
        public static IKernel Kernel { get; private set; }
        public static void Initialize()
        {
            Kernel = new StandardKernel(new NinjectModule());
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new Ninject.WebApi.DependencyResolver.NinjectDependencyResolver(Kernel);
        }
    }
}