﻿using System.Data.Entity;
using AutoParking.Api.Service;
using AutoParking.Model.Interfaces;
using AutoParking.Model.Models;
using AutoParking.Model.Repositories;
using Ninject.Web.Common;

namespace AutoParking.Api.DI
{
    public class NinjectModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IPaymentService>().To<PaymentService>();
            Bind<ITicketsRepository>().To<TicketsRepository>();
            Bind<IDebtsRepository>().To<DebtsRepository>();
            Bind<IPaymentsRepository>().To<PaymentsRepository>();
            Bind<IPaymentsFromApiRepository>().To<PaymentsFromApiRepository>();
            Bind<IPaymentLocksRepository>().To<PaymentLocksRepository>();
        }
    }
}