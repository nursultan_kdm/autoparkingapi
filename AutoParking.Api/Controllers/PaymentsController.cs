﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Xml.Serialization;
using AutoParking.Api.Enums;
using AutoParking.Api.Models;
using AutoParking.Api.Service;
using AutoParking.Common.Logger;
using AutoParking.Model.Repositories;
using Newtonsoft.Json;

namespace AutoParking.Api.Controllers
{
    public class PaymentsController : ApiController
    {
        private IPaymentService PaymentService { get; set; }

        public PaymentsController(IPaymentService paymentService)
        {
            PaymentService = paymentService;
        }

        [HttpPost]
        [Route("api/payments/pay")]
        public IHttpActionResult Pay(HttpRequestMessage request)
        {
            try
            {
                int cbkBankPaymentParkomatId = int.Parse(WebConfigurationManager.AppSettings["CbkBankPaymentParkomatId"]);
                PaymentRequestModel paymentRequest = null;
                XmlSerializer serializer = new XmlSerializer(typeof(PaymentRequestModel));
                using (Stream stream = request.Content.ReadAsStreamAsync().Result)
                {
                    paymentRequest = (PaymentRequestModel)serializer.Deserialize(stream);
                    Log.Info($"POST Payments/Pay: Model deserialized. Body: {JsonConvert.SerializeObject(paymentRequest)}");
                }

                if (paymentRequest.Head.Op.Equals(RequestType.QE11.ToString()))
                {
                    var response = PaymentService.ValidatePayment(paymentRequest);
                    Log.Info($"POST Payments/Pay: Id: {response.Head.Qid}, Type: {response.Head.Op}, Sum: {response.Body.Sum}, Status: {response.Body.Status}");
                    return Ok(response);
                }

                if (paymentRequest.Head.Op.Equals(RequestType.QE10.ToString()))
                {
                    var response = PaymentService.MakePayment(paymentRequest, cbkBankPaymentParkomatId);
                    Log.Info($"POST Payments/Pay: Id: {response.Head.Qid}, Type: {response.Head.Op}, Sum: {response.Body.Sum}, Status: {response.Body.Status}");
                    return Ok(response);
                }

                return BadRequest();
            }
            catch (Exception e)
            {
                Log.Error($"POST Payments/Pay. Exception: {e}");
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/payments/mobilnikpay")]
        public IHttpActionResult MobilnikPay(HttpRequestMessage request)
        {
            try
            {
                int mobilnikApiPaymentsParkomatId = int.Parse(WebConfigurationManager.AppSettings["MobilnikPaymentParkomatId"]);
                PaymentRequestModel paymentRequest = null;
                XmlSerializer serializer = new XmlSerializer(typeof(PaymentRequestModel));
                using (Stream stream = request.Content.ReadAsStreamAsync().Result)
                {
                    paymentRequest = (PaymentRequestModel)serializer.Deserialize(stream);
                    Log.Info($"POST Payments/MobilnikPay: Model deserialized. Body: {JsonConvert.SerializeObject(paymentRequest)}");
                }

                if (paymentRequest.Head.Op.Equals(RequestType.QE11.ToString()))
                {
                    var response = PaymentService.ValidatePayment(paymentRequest);
                    Log.Info($"POST Payments/MobilnikPay: Id: {response.Head.Qid}, Type: {response.Head.Op}, Sum: {response.Body.Sum}, Status: {response.Body.Status}");
                    return Ok(response);
                }

                if (paymentRequest.Head.Op.Equals(RequestType.QE10.ToString()))
                {
                    var response = PaymentService.MakePayment(paymentRequest, mobilnikApiPaymentsParkomatId);
                    Log.Info($"POST Payments/MobilnikPay: Id: {response.Head.Qid}, Type: {response.Head.Op}, Sum: {response.Body.Sum}, Status: {response.Body.Status}");
                    return Ok(response);
                }

                return BadRequest();
            }
            catch (Exception e)
            {
                Log.Error($"POST Payments/MobilnikPay. Exception: {e}");
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/payments/balancekgpay")]
        public IHttpActionResult BalanceKgPay(HttpRequestMessage request)
        {
            try
            {
                int balanceKgApiPaymentsParkomatId = int.Parse(WebConfigurationManager.AppSettings["BalanceKgPaymentParkomatId"]);
                PaymentRequestModel paymentRequest = null;
                XmlSerializer serializer = new XmlSerializer(typeof(PaymentRequestModel));
                using (Stream stream = request.Content.ReadAsStreamAsync().Result)
                {
                    paymentRequest = (PaymentRequestModel)serializer.Deserialize(stream);
                    Log.Info($"POST Payments/BalanceKgPay: Model deserialized. Body: {JsonConvert.SerializeObject(paymentRequest)}");
                }

                if (paymentRequest.Head.Op.Equals(RequestType.QE11.ToString()))
                {
                    var response = PaymentService.ValidatePayment(paymentRequest);
                    Log.Info($"POST Payments/BalanceKgPay: Id: {response.Head.Qid}, Type: {response.Head.Op}, Sum: {response.Body.Sum}, Status: {response.Body.Status}");
                    return Ok(response);
                }

                if (paymentRequest.Head.Op.Equals(RequestType.QE10.ToString()))
                {
                    var response = PaymentService.MakePayment(paymentRequest, balanceKgApiPaymentsParkomatId);
                    Log.Info($"POST Payments/BalanceKgPay: Id: {response.Head.Qid}, Type: {response.Head.Op}, Sum: {response.Body.Sum}, Status: {response.Body.Status}");
                    return Ok(response);
                }

                return BadRequest();
            }
            catch (Exception e)
            {
                Log.Error($"POST Payments/BalanceKgPay. Exception: {e}");
                return InternalServerError(e);
            }
        }
    }
}
