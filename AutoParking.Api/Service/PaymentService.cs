﻿using System;
using System.Linq;
using AutoParking.Api.Enums;
using AutoParking.Api.Helpers;
using AutoParking.Api.Models;
using AutoParking.Common.Logger;
using AutoParking.Model;
using AutoParking.Model.Calculators;
using AutoParking.Model.Enums;
using AutoParking.Model.Interfaces;
using Newtonsoft.Json;

namespace AutoParking.Api.Service
{
    public class PaymentService : IPaymentService
    {
        private ITicketsRepository TicketsRepository { get; set; }
        private IDebtsRepository DebtsRepository { get; set; }
        private IPaymentsRepository PaymentsRepository { get; set; }
        private IPaymentsFromApiRepository PaymentsFromApiRepository { get; set; }
        private IPaymentLocksRepository PaymentLocksRepository { get; set; }

        

        public PaymentService(ITicketsRepository ticketsRepository, IDebtsRepository debtsRepository, IPaymentsFromApiRepository paymentsFromApiRepository, IPaymentsRepository paymentsRepository, IPaymentLocksRepository paymentLocksRepository)
        {
            TicketsRepository = ticketsRepository;
            DebtsRepository = debtsRepository;
            PaymentsFromApiRepository = paymentsFromApiRepository;
            PaymentsRepository = paymentsRepository;
            PaymentLocksRepository = paymentLocksRepository;
        }

        public PaymentResponseModel ValidatePayment(PaymentRequestModel request)
        {
            Log.Info($"PaymentService.ValidatePayment: Request: {JsonConvert.SerializeObject(request)}");
            var response = new PaymentResponseModel();
            try
            {
                var ticket = TicketsRepository.FindAll().FirstOrDefault(x => x.TicketNumber.ToString().Equals(request.Body.AccountNumber) && x.IsEnabled);

                response.Head.Date = DateTime.Now;
                response.Head.Op = RequestType.QE11.ToString();
                response.Head.Qid = request.Head.Qid;
                response.Head.Qm = request.Head.Qm;
                response.Body.ServiceId = request.Body.ServiceId;

                if (PaymentsFromApiRepository.FindAll().Any(x => x.Qid == request.Head.Qid))
                {
                    response.Body.Status = (int)CommonStatuses.RequestInvalidFormat;
                    response.Body.Message = "Платеж уже существует в системе";

                    return response;
                }

                if (ticket != null)
                {
                    Log.Info($"PaymentService.ValidatePayment: Ticket found! TicketNumber: {ticket.TicketNumber}");
                    if (ticket.Zones.Sectors.IDSectorType == 1)
                    {
                        CarsState carState = ticket.CarsState.LastOrDefault(x => x.IDTicket == ticket.ID);
                        var amountTotal = ParkingCostCalculator.AmountTotal(carState, ticket);

                        if (amountTotal < 0)
                        {
                            amountTotal = 0;
                        }

                        response.Body.Sum = amountTotal;
                        response.Body.Status = (int) PaymentProcessingStatuses.ValidatePaymentOk;
                        

                        Log.Info($"PaymentService.ValidatePayment: Recommend amount to pay: {amountTotal}, SectorType: {ticket.Zones.Sectors.IDSectorType}, TicketNumber: {ticket.TicketNumber}");
                    }

                    if (ticket.Zones.Sectors.IDSectorType == 2)
                    {
                        var debtStatuses = new[] { (int)DebtStatuses.NotPaid, (int)DebtStatuses.PartiallyPaid };
                        var debt = DebtsRepository.FindAll().FirstOrDefault(x => x.IDTicket == ticket.ID && debtStatuses.Contains(x.Status));

                        if (debt != null)
                        {
                            Log.Info($"PaymentService.ValidatePayment: Debt found! TicketNumber: {ticket.TicketNumber}");

                            if (debt.CreationDate.Date == DateTime.Now.Date)
                            {
                                var paymentsAmount = ReservedParkingCostCalculator.PaymentsAmount(debt, ticket);

                                var amountToPay = debt.AmountToPay;

                                var amountToPayWithOldPayment = amountToPay - paymentsAmount;

                                var amountPenalty = debt.AmountPenalty;

                                //Тут просто приравниваем к нулю , если сумму ушла в минус (из за нашего кривого калькулятора)
                                if (amountToPayWithOldPayment < 0)
                                {
                                    amountPenalty = amountPenalty - Math.Abs(amountToPayWithOldPayment);
                                    amountToPayWithOldPayment = 0;
                                }

                                response.Body.Sum = amountToPayWithOldPayment + amountPenalty;
                                response.Body.Status = (int)PaymentProcessingStatuses.ValidatePaymentOk;
                                Log.Info($"PaymentService.ValidatePayment: Recommend amount to pay: {amountToPayWithOldPayment + amountPenalty}, SectorType: {ticket.Zones.Sectors.IDSectorType}, TicketNumber: {ticket.TicketNumber}");
                            }
                            else
                            {
                                response.Body.Status = (int)CommonStatuses.RequestInvalidFormat;
                                response.Body.ErrorMessage = "Задолженность просрочена! Обратитесь в администрацию рынка";
                                Log.Info($"PaymentService.ValidatePayment: Debt is overdue! TicketNumber: {ticket.TicketNumber}");
                            }
                        }
                        else
                        {
                            response.Body.Status = (int) CommonStatuses.AccountNotFound;
                            response.Body.ErrorMessage = "Задолженность не найдена";
                            Log.Info($"PaymentService.ValidatePayment: Debt is not found! TicketNumber: {ticket.TicketNumber}");
                        }

                    }

                }
                else
                {
                    response.Body.Status = (int) CommonStatuses.AccountNotFound;
                    response.Body.ErrorMessage = "Парковочный билет не найден";
                }
                return response;

            }
            catch (Exception e)
            {
                Log.Error($"PaymentService.ValidatePayment: Exception: {e}");
                response.Head.Date = DateTime.Now;
                response.Head.Op = request.Head.Op;
                response.Head.Qid = request.Head.Qid;
                response.Head.Qm = request.Head.Qm;
                response.Body.ServiceId = request.Body.ServiceId;

                response.Body.Status = (int) CommonStatuses.TemporarilyUnavailable;
                response.Body.ErrorMessage = "Сервис временно недоступен";
                
                return response;
            }
            
        }

        public PaymentResponseModel MakePayment(PaymentRequestModel request, int virtualParkomatId)
        {
            Log.Info($"PaymentService.MakePayment: Request: {JsonConvert.SerializeObject(request)}");
            var response = new PaymentResponseModel();
            try
            {

                var ticket = TicketsRepository.FindAll().FirstOrDefault(x => x.TicketNumber.ToString().Equals(request.Body.AccountNumber) && x.IsEnabled);

                response.Head.Date = DateTime.Now;
                response.Head.Op = RequestType.QE11.ToString();
                response.Head.Qid = request.Head.Qid;
                response.Head.Qm = request.Head.Qm;

                if (PaymentsFromApiRepository.FindAll().Any(x => x.Qid == request.Head.Qid))
                {
                    response.Body.Status = (int) CommonStatuses.RequestInvalidFormat;
                    response.Body.Message = "Платеж уже существует в системе";

                    return response;
                }

                if (ticket != null)
                {

                    Log.Info($"PaymentService.MakePayment: Ticket found! TicketNumber: {ticket.TicketNumber}");
                    if (ticket.Zones.Sectors.IDSectorType == 1)
                    {
                        CarsState carState = ticket.CarsState.LastOrDefault(x => x.IDTicket == ticket.ID);
                        var payment = new Payments();

                        var amountTotal = ParkingCostCalculator.AmountTotal(carState, ticket);

                        if (amountTotal <= 0)
                        {
                            response.Body.Status = (int)PaymentProcessingStatuses.SummTooHigh;
                            response.Body.Message = "Задолжность на текущий момент времени погашена";

                            return response;
                        }

                        payment.AmountTotal = ParkingCostCalculator.AmountTotal(carState, ticket);

                        var amountToPay = ParkingCostCalculator.DailyTotalAmount(carState, ticket);
                        var amountPenalty = ParkingCostCalculator.DailyPenalty(ticket);
                        var paymentsAmount = ParkingCostCalculator.PaymentsAmount(ticket);

                        
                        
                            //Высчитываем сумму из старых платежеи 
                            //Сначала высчитываем сумму за аренду парковки
                            //Затем из суммы штрафа
                            var amountToPayWithOldPayment = amountToPay - paymentsAmount;

                        payment.AmountPenalty = amountPenalty;

                        //Тут просто приравниваем к нулю , если сумму ушла в минус (из за нашего кривого калькулятора)
                        if (amountToPayWithOldPayment < 0)
                        {
                            payment.AmountPenalty = amountPenalty - Math.Abs(amountToPayWithOldPayment);
                            amountToPayWithOldPayment = 0;
                        }

                        payment.AmountToPay = amountToPayWithOldPayment;
                        payment.AmountEntered = request.Body.Sum;

                        payment.CreationDate = DateTime.Now;
                        payment.Guid = Guid.NewGuid();
                        payment.Status = (int)PaymentStatuses.Completed;
                        payment.TicketID = ticket.ID;
                        payment.Tariff = ticket.Zones.Tariff.ToString();
                        payment.ParkomatID = virtualParkomatId;

                        using (new PaymentLocker(PaymentLocksRepository, request.Head.Qid))
                        {
                            PaymentsRepository.Insert(payment);
                            PaymentsRepository.Save();

                            var paymentApi = new PaymentsFromApi();
                            paymentApi.Id = payment.ID;
                            paymentApi.Qid = request.Head.Qid;
                            paymentApi.Qm = request.Head.Qm;
                            paymentApi.ServiceIdentificator = request.Body.ServiceId.ToString();

                            PaymentsFromApiRepository.Insert(paymentApi);
                            PaymentsFromApiRepository.Save();
                        }

                        ticket.ParkingPaidDate = CountParkingPaidDate(ticket);
                        if (request.Body.Sum >= payment.AmountTotal)
                        {
                            ticket.PenaltyBeginDate = ticket.ParkingPaidDate;
                        }
                        TicketsRepository.Save();

                        response.Body.Status = (int)PaymentProcessingStatuses.PaymentSucces;
                        response.Body.Message = "Платеж проведен";

                        Log.Info($"PaymentService.MakePayment: Payment saved! Recommend amount to pay: {payment.AmountTotal}, SectorType: {ticket.Zones.Sectors.IDSectorType}, TicketNumber: {ticket.TicketNumber}");
                    }

                    if (ticket.Zones.Sectors.IDSectorType == 2)
                    {
                        var debtStatuses = new[] { (int)DebtStatuses.NotPaid, (int)DebtStatuses.PartiallyPaid };
                        var debt = DebtsRepository.FindAll().FirstOrDefault(x => x.IDTicket == ticket.ID && debtStatuses.Contains(x.Status));

                        if (debt != null)
                        {
                            var payment = new Payments();

                            Log.Info($"PaymentService.MakePayment: Debt found! TicketNumber: {ticket.TicketNumber}");

                            if (debt.CreationDate.Date == DateTime.Now.Date)
                            {
                                var paymentsAmount = ReservedParkingCostCalculator.PaymentsAmount(debt, ticket);

                                var amountToPay = debt.AmountToPay;

                                var amountToPayWithOldPayment = amountToPay - paymentsAmount;

                                payment.AmountPenalty = debt.AmountPenalty;

                                //Тут просто приравниваем к нулю , если сумму ушла в минус (из за нашего кривого калькулятора)
                                if (amountToPayWithOldPayment < 0)
                                {
                                    payment.AmountPenalty = payment.AmountPenalty - Math.Abs(amountToPayWithOldPayment);
                                    amountToPayWithOldPayment = 0;
                                }

                                if (amountToPayWithOldPayment + debt.AmountPenalty <= 0)
                                {
                                    response.Body.Status = (int)PaymentProcessingStatuses.SummTooHigh;
                                    response.Body.Message = "Задолжность на текущий момент времени погашена";

                                    return response;
                                }

                                payment.AmountToPay = amountToPayWithOldPayment;
                                payment.AmountTotal = payment.AmountToPay + payment.AmountPenalty;
                                payment.AmountEntered = request.Body.Sum;

                                payment.CreationDate = DateTime.Now;
                                payment.Guid = Guid.NewGuid();
                                payment.IDDebt = debt.ID;
                                payment.Status = (int)PaymentStatuses.Completed;
                                payment.TicketID = ticket.ID;
                                payment.Tariff = ticket.Zones.Tariff.ToString();
                                payment.ParkomatID = virtualParkomatId;

                                using (new PaymentLocker(PaymentLocksRepository, request.Head.Qid))
                                {
                                    PaymentsRepository.Insert(payment);
                                    PaymentsRepository.Save();

                                    var paymentApi = new PaymentsFromApi();
                                    paymentApi.Id = payment.ID;
                                    paymentApi.Qid = request.Head.Qid;
                                    paymentApi.Qm = request.Head.Qm;
                                    paymentApi.ServiceIdentificator = request.Body.ServiceId.ToString();

                                    PaymentsFromApiRepository.Insert(paymentApi);
                                    PaymentsFromApiRepository.Save();
                                }

                                ticket.ParkingPaidDate = ReservedParkingCostCalculator.CalculatePaidDate(ticket);

                                if (payment.AmountEntered >= payment.AmountTotal)
                                {
                                    ticket.PenaltyBeginDate = ticket.ParkingPaidDate;
                                    debt.Status = (int)DebtStatuses.Paid;
                                }
                                else
                                {
                                    debt.Status = (int)DebtStatuses.PartiallyPaid;
                                }

                                DebtsRepository.Save();
                                TicketsRepository.Save();

                                response.Body.Status = (int)PaymentProcessingStatuses.PaymentSucces;
                                response.Body.Message = "Платеж проведен";


                                Log.Info($"PaymentService.MakePayment: Payment saved! Recommend amount to pay: {amountToPayWithOldPayment + payment.AmountTotal}, SectorType: {ticket.Zones.Sectors.IDSectorType}, TicketNumber: {ticket.TicketNumber}");
                            }
                            else
                            {
                                response.Body.Status = (int)CommonStatuses.RequestInvalidFormat;
                                response.Body.ErrorMessage = "Задолженность просрочена! Обратитесь в администрацию рынка";
                                Log.Info($"PaymentService.MakePayment: Debt is overdue! TicketNumber: {ticket.TicketNumber}");
                            }
                        }
                        else
                        {
                            response.Body.Status = (int)CommonStatuses.AccountNotFound;
                            response.Body.ErrorMessage = "Задолженность не найдена";
                            Log.Info($"PaymentService.MakePayment: Debt is not found! TicketNumber: {ticket.TicketNumber}");
                        }

                    }

                }
                else
                {
                    Log.Info($"PaymentService.MakePayment: Ticket not found! TicketNumber: {request.Body.AccountNumber}");
                    response.Body.Status = (int)CommonStatuses.AccountNotFound;
                    response.Body.ErrorMessage = "Парковочный билет не найден";
                }

                return response;

            }
            catch (Exception e)
            {
                Log.Error($"PaymentService.MakePayment: Exception: {e}");
                response.Head = new Head()
                {
                    Date = DateTime.Now,
                    Op = request.Head.Op,
                    Qid = request.Head.Qid,
                    Qm = request.Head.Qm
                };
                response.Body = new Body
                {
                    Status = (int) CommonStatuses.TemporarilyUnavailable,
                    ErrorMessage = "Сервис временно недоступен"
                };
                return response;
            }
        }

        private DateTime CountParkingPaidDate(Tickets ticket)
        {
            int DayBeforeBeginСountPenalty = 14;
            var penaltyBeginDate = ticket.PenaltyBeginDate.AddDays(DayBeforeBeginСountPenalty);
            var penaltyStartBeginDate = ticket.PenaltyBeginDate;

            var toDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            var ticketPayedAmount = PaymentsRepository.FindAll().Where(p => p.TicketID == ticket.ID && p.CreationDate >= ticket.PenaltyBeginDate).Sum(p => p.AmountEntered - p.AmountChange);

            decimal totalAmount = 0;

            while (penaltyStartBeginDate <= toDate)
            {

                decimal amount = ticket.Zones.Tariff.Prices.First(p => (p.Weekday + 1) % 7 == (int)penaltyStartBeginDate.DayOfWeek).Amount;

                totalAmount += amount;

                //Прибавляем к общей сумме задолжностей, сумму штрафов
                if (penaltyStartBeginDate >= penaltyBeginDate)
                {
                    totalAmount += amount;
                }

                if (ticketPayedAmount >= totalAmount)
                {
                    penaltyStartBeginDate = penaltyStartBeginDate.AddDays(1);
                }
                else
                {
                    break;
                }
            }

            return penaltyStartBeginDate;
        }
    }
}