﻿using AutoParking.Api.Models;

namespace AutoParking.Api.Service
{
    public interface IPaymentService
    {
        PaymentResponseModel ValidatePayment(PaymentRequestModel request);
        PaymentResponseModel MakePayment(PaymentRequestModel request, int virtualParkomatId);
    }
}