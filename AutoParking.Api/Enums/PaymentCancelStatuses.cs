﻿namespace AutoParking.Api.Enums
{
    public enum PaymentCancelStatuses
    {
        PaymentCancelSuccess = 250,
        InProcess = 400,
        BalanceIsTooLow = 420
    }
}