﻿namespace AutoParking.Api.Enums
{
    public enum CommonStatuses
    {
        RequestInvalidFormat = 401,
        AccountNotFound = 420,
        TemporarilyUnavailable = 424
    }
}