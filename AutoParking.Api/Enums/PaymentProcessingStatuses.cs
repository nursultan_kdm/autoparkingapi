﻿namespace AutoParking.Api.Enums
{
    public enum PaymentProcessingStatuses
    {
        ValidatePaymentOk = 200,
        PaymentSucces = 250,
        InProcess = 400,
        SummTooHigh = 422,
        SummTooLow = 423
    }
}