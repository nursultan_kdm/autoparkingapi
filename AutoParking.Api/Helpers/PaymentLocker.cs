﻿using System;
using System.Configuration;
using System.Linq;
using AutoParking.Common.Logger;
using AutoParking.Model;
using AutoParking.Model.Interfaces;

namespace AutoParking.Api.Helpers
{
    public class PaymentLocker : IDisposable
    {
        private IPaymentLocksRepository PaymentLocksRepository { get; set; }
        private bool IsLocked { get; set; }
        private string Guid { get; set; }
        public PaymentLocker(IPaymentLocksRepository paymentLocksRepository, string guid)
        {
            PaymentLocksRepository = paymentLocksRepository;
            Guid = guid;
            int configTime = int.Parse(ConfigurationManager.AppSettings["LockTimeoutInSeconds"]);
            try
            {
                PaymentLocksRepository.DeleteOldData(configTime);
                var paymentLock = new PaymentLocks
                {
                    date = DateTime.Now,
                    guid = guid
                };
                PaymentLocksRepository.Insert(paymentLock);
                PaymentLocksRepository.Save();
                IsLocked = true;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw new Exception("Cannot lock payment for exclusive access");
            }
        }

        public void Dispose()
        {
            if (IsLocked)
            {
                var paymentLockItem = PaymentLocksRepository.FindAll().FirstOrDefault(x => x.guid.Equals(Guid));
                PaymentLocksRepository.Delete(paymentLockItem);
                PaymentLocksRepository.Save();
            }
        }
    }
}